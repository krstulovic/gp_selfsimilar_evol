# Circulation in HVBK

Text and scripts for circulation at finite temperature using the GP, HVBK and NS models.

## Compilation with latexmk

Compile using `latexmk -pdf text.tex`

To clean the auxiliary files use `latexmk -c` or `latexmk -C` if you also want to delete de pdf file.

## Compare .tex files

Start by getting the version of a file from a previous commit, with

`git show <commit>:text.tex > tmp.tex`

Run latexdiff to generate a new .tex file with the differences highlighted:

`latexdiff tmp.tex text.tex > diff.tex`

Compile diff.tex as usual.

For more info:
https://tex.stackexchange.com/questions/266879/latex-git-mark-differences-since-specific-commit
 


